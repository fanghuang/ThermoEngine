import numpy as np
from os import path
import pandas as pd
import scipy.optimize as opt
from scipy import optimize
import scipy.linalg as lin
import scipy as sp
import sys
import sympy as sym

from collections import OrderedDict as odict

from thermoengine import coder, core, phases, model, equilibrate


def get_subsolidus_phases(database='Berman', include_metal=False, 
                          return_model=False):
    remove_phases = ['Liq','H2O']
    
    if not include_metal:
        remove_phases.extend(['MtlS', 'MtlL'])

    modelDB = model.Database(database)

    phases = modelDB.phases
    # [phases.pop(phs) for phs in remove_phases]
    
    if return_model:
        return phases, modelDB
    else:
        return phases
        


def system_energy_landscape(T, P, phases, prune_polymorphs=True, TOL=1e-3):
    elem_comps = []
    phs_sym = []
    endmem_ids = []
    endmem_names = []
    mu = []
    phase_comps = odict()
    phase_endmems = odict()
    for phsnm in phases:
        phs = phases[phsnm]

        iphs_details = odict()

        elem_comp = phs.props['element_comp']
        abbrev = phs.abbrev
        endmem_num = phs.endmember_num
        iendmem_ids = list(np.arange(endmem_num))
        iendmem_names = list(phs.endmember_names)
        imu = []

        if phs.phase_type=='pure':
            nelem = np.sum(elem_comp)
            imu += [phs.gibbs_energy(T, P)/nelem]
            # print(nelem)
        else:
            nelem = np.sum(elem_comp,axis=1)
            # print(nelem)
            for i in iendmem_ids:
                imol = np.eye(phs.endmember_num)[i]
                imu += [phs.gibbs_energy(T, P, mol=imol,deriv={"dmol":1})[0,i]/nelem[i]]
                # print(nelem[i])

        endmem_names.extend(iendmem_names)
        endmem_ids.extend(iendmem_ids)
        phs_sym.extend(list(np.tile(abbrev,endmem_num)))
        # print(elem_comp)

        iphs_details['endmem_comp'] = elem_comp
        iphs_details['endmem_mu'] = np.array(imu)
        iphs_details['endmem_names'] = iendmem_names

        phase_comps[phsnm] = elem_comp
        # phase_mu_endmem[phsnm] = np.array(imu)

        mu.extend(imu)
        elem_comps.extend(elem_comp)
        # print(elem_comp)
        # print(phs)
        phase_endmems[phsnm] = iphs_details

    elem_comps = np.vstack(elem_comps)

    natoms = np.sum(elem_comps,axis=1)
    elem_comps = elem_comps/natoms[:,np.newaxis]

    elem_mask = ~np.all(elem_comps<TOL, axis=0)

    for phsnm in phase_comps:
        icomp = phase_comps[phsnm]
        inatom = np.sum(icomp, axis=1)
        inorm_comp = icomp[:,elem_mask]/inatom[:,np.newaxis]
        phase_comps[phsnm] = inorm_comp
        phase_endmems[phsnm]['endmem_comp'] = inorm_comp
        # phase_comps[phsnm] = icomp[:,elem_mask]

    elem_comps = elem_comps[:, elem_mask]
    mu = np.array(mu)
    endmem_names = np.array(endmem_names)
    endmem_ids = np.array(endmem_ids)

    sys_elems = core.chem.PERIODIC_ORDER[elem_mask]

    if prune_polymorphs:
        phs_sym, endmem_ids, mu, elem_comps = (
            remove_polymorphs(phs_sym, endmem_ids, mu, elem_comps))

    phs_sym = np.array(phs_sym)

    # Store initial chempot as all zero reference
    phase_endmems['chempot'] = np.zeros(elem_mask.sum())

    # phase_comps, phase_mu_endmem
    return (phs_sym, endmem_ids, endmem_names, mu, elem_comps,
            sys_elems, phase_endmems)

def remove_polymorphs(phs_sym, endmem_ids, mu, elem_comps, decimals=4):
    elem_round_comps = np.round(elem_comps, decimals=decimals)
        # Drop identical comps
    elem_comps_uniq = np.unique(elem_round_comps, axis=0)

    # uniq_num = elem_comps_uniq.shape[0]
    mu_uniq = []
    phs_sym_uniq = []
    endmem_ids_uniq = []
    for elem_comp in elem_comps_uniq:
        is_equiv_comp = np.all(elem_round_comps == elem_comp[np.newaxis,:], axis=1)
        equiv_ind = np.where(is_equiv_comp)[0]
        min_ind = equiv_ind[np.argsort(mu[equiv_ind])[0]]
        min_mu = mu[min_ind]
        assert np.all(min_mu <= mu[equiv_ind]), 'fail'

        mu_uniq.append(min_mu)
        phs_sym_uniq.append(phs_sym[min_ind])
        endmem_ids_uniq.append(endmem_ids[min_ind])

    mu_uniq = np.array(mu_uniq)
    phs_sym_uniq = np.array(phs_sym_uniq)
    elem_comps_uniq = np.array(elem_comps_uniq)

    return phs_sym_uniq, endmem_ids_uniq, mu_uniq, elem_comps_uniq

def reduce_rank_comp(comp, ignore_oxy=True):
    if ignore_oxy:
        if comp.ndim == 1:
            comp_fit = comp[1:]
        elif comp.ndim == 2:
            comp_fit = comp[:,1:]
        else:
            assert False, ('That is not a valid comp array. ',
                           'It can only have 1 or 2 dimensions.')

    else:
        comp_fit = comp

    return comp_fit

def min_energy_linear_assemblage(bulk_comp, comp, mu, ignore_oxy=True,
                                 TOLmu=10, TOL=1e-5):

    bulk_comp_fit = reduce_rank_comp(bulk_comp, ignore_oxy=ignore_oxy)
    comp_fit = reduce_rank_comp(comp, ignore_oxy=ignore_oxy)

    xy = np.hstack((comp_fit, mu[:,np.newaxis]))
    yavg = np.mean(mu)
    xy_bulk = np.hstack((bulk_comp_fit, yavg))

    wt0, rnorm0 = opt.nnls(xy.T, xy_bulk)
    # print('rnorm',rnorm0)

    def fun(mu, shift=0):
        xy_bulk[-1] = mu
        wt, rnorm = opt.nnls(xy.T, xy_bulk)
        return rnorm-shift


    delmu = .1
    if rnorm0==0:
        shift_dir = -1
        soln_found = True
    else:
        output = opt.minimize_scalar(fun, bounds=[np.min(mu), np.max(mu)])
        xy_bulk[-1] = output['x']
        wt0, rnorm0 = opt.nnls(xy.T, xy_bulk)
        shift_dir = -1

    mu_prev=xy_bulk[-1]
    rnorm=rnorm0

    while True:
        mu_prev = xy_bulk[-1]
        rnorm_prev = rnorm

        xy_bulk[-1] += shift_dir*delmu
        wt, rnorm = opt.nnls(xy.T, xy_bulk)
        delmu *= 2

        # print(shift_dir, rnorm)
        if ((shift_dir==+1)&(rnorm>rnorm_prev)) or ((shift_dir==-1)&(rnorm>0)):
            break


    fun_fit = lambda mu, TOL=TOL: fun(mu, shift=TOL)
    if rnorm > TOL:
        mu_bulk = opt.brentq(fun_fit, mu_prev, xy_bulk[-1], xtol=TOLmu)
        xy_bulk[-1] = mu_bulk
        wt, rnorm = opt.nnls(xy.T, xy_bulk)

    mu_bulk = xy_bulk[-1]
    wt_bulk = wt

    ind_assem = np.where(wt_bulk>0)[0]
    chempot = fit_chempot(ind_assem, bulk_comp_fit, mu,
                          comp_fit, ignore_oxy=ignore_oxy)

    return chempot, mu_bulk, wt_bulk, ind_assem

def phase_affinity(chempot, phases, phase_endmems,T, P, verbose=False,
                   correct_aff=False, debug=False, method='special'):
    Nphs = len(phases)
    mu_soln = []
    # A = []
    phase_names = []

    A = odict()
    X = odict()
    mu_soln = odict()
    comp_soln = odict()
    natom = odict()
    dmudn_soln = odict()

    for iphs_nm, iphs in phases.items():
        if verbose:
            print(iphs_nm)
        # print(iphs_nm,iphs)
        # print(iphs)
        phase_names.append(iphs_nm)
        iphs_endmem = phase_endmems[iphs_nm]

        icomp = iphs_endmem['endmem_comp']
        inatom = iphs.props['atom_num']

        # icomp *= inatom[:,np.newaxis]
        # print(icomp)
        ichempot = np.dot(icomp*inatom[:,np.newaxis], chempot)
        # print(ichempot)
        # iA, iX = iphs.affinity_and_comp(T, P, ichempot, debug=debug)
        iA, iX = iphs.affinity_and_comp(T, P, ichempot, debug=debug, method=method)
        if np.isnan(iA):
            ichempot_str = np.array2string(ichempot, separator=', ',max_line_width=1000000)
            print('A NAN value was found by affinity and comp. Try to fix this now...')
            print('phs = modelDB.get_phase(\''+iphs_nm+'\')')
            print("A, X = phs.affinity_and_comp(",T,",",P,", np.array(",ichempot_str,
                  "), debug=True, method='special')")
            from IPython import embed; embed()

        #icomp /= inatom[:,np.newaxis]
        icomp_soln = np.dot(iX, icomp)
        # print('icomp_soln tot = ', icomp_soln.sum())

        X[iphs_nm] = iX
        inatom_scl = np.dot(inatom,iX)

        if iX.size==1:
            iX = None



        natom[iphs_nm] = inatom
        imu = np.squeeze(iphs.chem_potential(T,P,mol=iX))
        # imu /= iphs.props['atom_num']

        # if imu.size==1:
        #     imu = [imu]

        # imu = iphs.gibbs_energy(T,P,mol=iX, deriv={'dmol':1})

        # print(imu)
        if iX is None:
            id2Gdm2 = np.array([[0]])
        else:
            id2Gdm2 = np.squeeze(iphs.gibbs_energy(
                T, P, mol=iX, deriv={'dmol':2}))
            # print(np.dot(id2Gdm2, iX))
            # idn = 2*np.random.rand(iX.size)-1
            # print(np.dot(id2Gdm2,idn))
            # print('---')


        iA_scl = iA/inatom_scl
        imu_scl = imu/inatom

        if iX is None:
            iGsoln_mu = imu_scl[0]
        else:
            iGsoln_mu = np.dot(imu_scl, iX)
            iGsoln = iphs.gibbs_energy(T, P, mol=iX)/inatom_scl
            if verbose:
                print('dG = ', iGsoln-iGsoln_mu)

        iGsoln_aff = iA_scl + np.dot(icomp_soln, chempot)

        if verbose:
            print('iX', iX)
            print('iGsoln diff = ', iGsoln_aff-iGsoln_mu)

        if correct_aff:
            iA = iGsoln_mu - np.dot(icomp_soln, chempot)



        # print(id2Gdm2)
        # scale by natom
        mu_soln[iphs_nm] = imu/inatom
        dmudn_soln[iphs_nm] = id2Gdm2


        A[iphs_nm] = iA/inatom_scl
        comp_soln[iphs_nm] = icomp_soln
        # print(id2Gdm2.squeeze())


        # print(np.squeeze(imu))
        # print(iX)
        # A.append(iA)
        # mu_soln.extend(imu)

    # A = np.array(A)
    # mu_soln = np.array(mu_soln)
    phase_names = np.array(phase_names)

    return mu_soln, dmudn_soln, comp_soln, X, A, natom,phase_names

def update_phase_affinities(chempot, T, P, phases, phase_endmems,
                            verbose=False, correct_aff=False, debug=False):

    (mu_soln, dmudn_soln, comp_soln_d, X_soln, A_d, natom,
     phase_names_soln) =  phase_affinity(
         chempot, phases, phase_endmems,T, P, verbose=verbose,
         correct_aff=correct_aff, debug=debug)
    extras = {'mu0':mu_soln, 'dmudn':dmudn_soln, 'X':X_soln,
              'elem_comp':comp_soln_d, 'phase_endmems':phase_endmems}

    Aff_soln = np.array(list(A_d.values()))
    comp_soln = np.array(list(comp_soln_d.values()))


    # Aff_endmem = []
    phase_props = phase_endmems.copy()
    chempot_prev = phase_props['chempot']
    dchempot = chempot-chempot_prev
    if verbose:
        print('dchempot = ', dchempot)

    phase_props['chempot'] = chempot

    for iphs in A_d:
        iprops = phase_props[iphs]

        iX_prev = iprops['X']
        iAff_prev = iprops['Aff']
        icomp_prev = iprops['comp']
        imu_prev = iprops['mu']


        # print('---')
        # print(iphs)
        # print('iX = ', iX_prev)
        # print('iAff = ', iAff_prev)
        # print('icomp = ', icomp_prev)
        # print('imu = ', imu_prev)

        if iprops['endmem_num']==1:
            iAff_update = iAff_prev - np.dot(dchempot, icomp_prev.T)
            # print('iAff update (perturb)= ', iAff_update)
            phase_props[iphs]['Aff'] = iAff_update
            continue

        iA_new = A_d[iphs]
        iX_new = X_soln[iphs]
        icomp_new = comp_soln_d[iphs]

        imu_endmem_curr = mu_soln[iphs]
        imu_curr = np.dot(iX_new, imu_endmem_curr)

        imu_new = iA_new + np.dot(chempot, icomp_new)
        if verbose:
            print('mu_curr_error = ', imu_new - imu_curr)

        # imask = phs_sym==iphs
        # iN = iX.size


        # NOTE: this is wrong I think. It needs to use dchempot, not chempot
        # iAff_prev_update = imu_prev - np.dot(chempot, icomp_prev.T)
        # print('iAff update (absolute)= ', iAff_prev_update)

        # Relative adjustment calculation now matches absolute calculation
        # for solution phases
        # assuming that externally initialized properly
        iAff_prev_update = iAff_prev - np.dot(dchempot, icomp_prev.T)
        # print('iAff update (perturb)= ', iAff_prev_update)

        # print('comp_prev = ', icomp_prev)

        iX_update = np.vstack((iX_prev, iX_new))
        iAff_update = np.hstack((iAff_prev_update, iA_new))
        icomp_update = np.vstack((icomp_prev, icomp_new))

        # print('imu_prev = ', imu_prev)
        # print('imu_new = ', imu_new)
        imu_update = np.hstack((imu_prev, imu_new))

        # Aff_endmem.append(np.tile(iA, iN))
        # iendmem_comp = phase_endmems[iphs]['endmem_comp']
        # iendmem_mu = phase_endmems[iphs]['endmem_mu']

        # iA_endmem = iendmem_mu - np.dot(chempot, iendmem_comp.T)

        # phase_props[iphs]['X'] = iX
        # phase_props[iphs]['Aff'] = iA
        # phase_props[iphs]['Aff_endmem'] = iA_endmem
        # phase_props[iphs]['comp'] = ielem_comp

        phase_props[iphs]['X'] = iX_update
        phase_props[iphs]['Aff'] = iAff_update
        phase_props[iphs]['comp'] = icomp_update
        phase_props[iphs]['mu'] = imu_update


        # dmu_endmem[imask] = iA+Ashft

    # Aff_endmem = np.hstack(Aff_endmem)

    # extras['Aff']
    # return phase_props, comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras
    return phase_props, comp_soln, X_soln, Aff_soln, phase_names_soln, extras


def fit_chempot(ind_assem, bulk_comp_fit, mu_compounds,
                comp_compounds_fit, ignore_oxy=True):
    mu_assem = mu_compounds[ind_assem]
    comp_assem_fit = comp_compounds_fit[ind_assem,:]

    output = np.linalg.lstsq(comp_assem_fit, mu_assem, rcond=-1)

    if ignore_oxy:
        chempot = np.hstack((0, output[0]))
    else:
        chempot = output[0]

    return chempot

def init_assem_props(T, P, phs_sym_compound, wt_bulk_compound, comp_compound,
                     phase_comps, phases):

    mu_assem0 = {}
    dmudn_assem0 = {}
    nmol_assem0 = {} #
    endmem_id_assem0 = {} #

    assem_phases = np.unique(phs_sym_compound[wt_bulk_compound>0])
    phs_sym_assem0 = assem_phases.copy() #

    for iphs_sym in assem_phases:
        imask_phs = phs_sym_compound==iphs_sym
        icomp_phs = comp_compound[imask_phs]
        iwt = wt_bulk_compound[imask_phs]
        iphs = phases[iphs_sym]

        # iX = X_soln[iphs_sym]

        iphs_endmem_comps = phase_comps[iphs_sym]

        # assemble mols of total soln phase from endmems and current equil soln comp
        iphs_comp = np.dot(iwt, icomp_phs)
        ioutput = np.linalg.lstsq(iphs_endmem_comps.T, iphs_comp, rcond=-1)
        iendmem_mols = ioutput[0]

        # print(iphs_sym)
        # print('iX', iX)
        # print('comp = ',iphs_comp)
        # print('mol endmem = ', iendmem_mols)
        # print('endmems = ', iphs.endmember_names)
        # print('endmem comps = ', iphs_endmem_comps)
        # print(iphs)

        ielems = iphs.props['element_comp']
        inum_atoms = ielems.sum(axis=1)

        # print('num atoms = ', inum_atoms)

        if iphs.endmember_num > 1:
            ichempot = iphs.chem_potential(T, P, mol=iendmem_mols).squeeze()/inum_atoms
            idmudn = iphs.gibbs_energy(T, P, mol=iendmem_mols, deriv={'dmol':2}).squeeze()
        else:
            ichempot = iphs.gibbs_energy(T, P).squeeze()/inum_atoms
            idmudn = np.array([[0]])





        # print('chempot = ', ichempot)



        # nmol_assem0.extend(iendmem_mols)
        # phs_sym_assem0.extend([iphs_sym for i in range(iphs.endmember_num)])
        # endmem_id_assem0.extend(range(iphs.endmember_num))
        # mu_assem0.extend(list(ichempot))

        nmol_assem0[iphs_sym] = iendmem_mols
        endmem_id_assem0[iphs_sym] = np.arange(iphs.endmember_num)
        mu_assem0[iphs_sym] = ichempot
        dmudn_assem0[iphs_sym] = idmudn

        # print('---')


    # print('=====')
    # print('nmol_assem0 = ', nmol_assem0)
    # print('phs_sym_assem0 = ', phs_sym_assem0)
    # print('endmem_id_assem0 = ', endmem_id_assem0)
    # print('mu_assem0 = ', mu_assem0)
    # print('dmudn_assem0 = ', dmudn_assem0)
    #
    # phs_sym_assem0
    assem_props = {}

    assem_props['nmol'] = nmol_assem0
    assem_props['phs_sym'] = phs_sym_assem0
    assem_props['endmem_id'] = endmem_id_assem0
    assem_props['mu0'] = mu_assem0
    assem_props['dmudn0'] = dmudn_assem0

    return assem_props

def get_linear_phase_model(dmudn, X, mu0):

    # M_tot = np.zeros((Nendmem, Nendmem))
    # dndmu_tot = np.zeros((Nendmem, Nendmem))
    dmudn_tot = np.zeros((Nendmem, Nendmem))
    nmol_end0 = np.zeros(Nendmem)
    mu0_endmem = np.zeros(Nendmem)

    ind_endmem = 0
    for phsnm in mu0:
        idmudn = dmudn[phsnm]
        iX = X[phsnm]
        iendmem_num = iX.size

        imu0 = mu0[phsnm]
        # icomp = comp[phsnm]
        # iphs_comp = phase_comps[phsnm]

        # idndmu = np.linalg.pinv(idmudn)
        # iM = np.dot(idmudn, idndmu)
        #
        # M_tot[ind_endmem:ind_endmem+iendmem_num,
        #       ind_endmem:ind_endmem+iendmem_num] = iM

        # dndmu_tot[ind_endmem:ind_endmem+iendmem_num,
        #           ind_endmem:ind_endmem+iendmem_num] = idndmu
        dmudn_tot[ind_endmem:ind_endmem+iendmem_num,
                  ind_endmem:ind_endmem+iendmem_num] = idmudn

        nmol_end0[ind_endmem:ind_endmem+iendmem_num] = iX

        mu0_endmem[ind_endmem:ind_endmem+iendmem_num] = imu0

        ind_endmem += iendmem_num

    return mu0_endmem, nmol_end0, dmudn_tot

def refine_assemblage(bulk_comp, chempot_init, mu0, dmudn, X):

    mu0_endmem, nmol_end0, dmudn_tot  = get_linear_phase_model(dmudn, X, mu0)

    A, B = get_linear_constraints(bulk_comp, Gtot_init, mu0_endmem,
                                  nmol_end0, dmudn_tot,
                                  phase_endmem_num, comp_endmem)

    B['Gtot'] = Gtot_init-Gshft


    A_lsq, B_lsq = build_lstsq(A, B, Affwt_endmem, phase_endmem_num,
                               sig_nmol_prior=sig_nmol_prior,
                               sig_comp=sig_comp, sig_mu=sig_mu)

    output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
    params = output[0]
    chempot_fit = params[:Nelems]
    nmol_fit = params[Nelems:]

def discover_approx_hull(bulk_comp, chempot_init, Gtot_init, Aff_phases_init,
                         mu0, dmudn, X, phase_endmem_num, comp_endmem,
                         debug=False, muTOL=0.1, sig_nmol_prior=.2,
                         sig_comp=.01, sig_mu=10, Ascl_min=1, Ascl_max=1e3,
                         Nsteps=10, max_iter=50, Gshft=1e3):

    Aff_phases = Aff_phases_init
    Npure = np.sum(phase_endmem_num==1)

    # Gtot_init = np.dot(chempot_init, bulk_comp)

    mu0_endmem, nmol_end0, dmudn_tot  = get_linear_phase_model(dmudn, X, mu0)


    A, B = get_linear_constraints(bulk_comp, Gtot_init, mu0_endmem,
                                  nmol_end0, dmudn_tot,
                                  phase_endmem_num, comp_endmem)

    B['Gtot'] = Gtot_init-Gshft

    Ascl_thresh = Ascl_max
    nmol = nmol_end0.copy()
    chempot = chempot_init.copy()


    Ascl_fac = np.exp(np.log(Ascl_max/Ascl_min)/Nsteps)


    count=1
    while True:

        # Affwt_endmem, Affwt = get_affinity_wt(Aff_phases, phase_endmem_num, Ascl_thresh)
        Affwt_endmem, Affwt = get_affinity_wt(Aff_phases_init, phase_endmem_num, Ascl_thresh)
        A_lsq, B_lsq = build_lstsq(A, B, Affwt_endmem, phase_endmem_num,
                                   sig_nmol_prior=sig_nmol_prior,
                                   sig_comp=sig_comp, sig_mu=sig_mu)

        output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
        params = output[0]
        chempot_fit = params[:Nelems]
        nmol_fit = params[Nelems:]
        Aff_phases_fit = update_fitted_affinities(
            params, A, B, phase_endmem_num)
        dnmol = nmol_fit-nmol
        dchempot = chempot_fit-chempot

        resid = np.dot(A_lsq, params)-B_lsq
        chisqr = np.sqrt(np.mean(resid**2))


        if debug:
            print('chi2 = ', chisqr)
            print('Aff_phases_fit = ', Aff_phases_fit)
            print('sig. wt count = ', np.sum(Affwt_endmem>.05))
            print('Ascl_thresh = ', Ascl_thresh)
            print(np.max(dnmol))
            print('    dchempot = ',np.max(np.abs(dchempot)))
            print('----------')

        # if np.all(np.abs(dnmol) < TOL):
        #     break

        if count >= max_iter:
            break

        if np.all(np.abs(dchempot) < muTOL):
            break

        Aff_phases = Aff_phases_fit
        nmol = nmol_fit
        chempot = chempot_fit

        if Ascl_thresh > Ascl_min:
            Ascl_thresh /= Ascl_fac

        count += 1

    return chempot_fit, nmol_fit, Aff_phases_fit, Affwt
