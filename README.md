# ThermoEngine README
The ThermoEngine repository contains Python packages, C code and header files and C++ and Objective-C class implementations that implement thermodynamic properties of minerals, fluids, and melts.  The repository also includes generic phase equilibrium calculators as well as phase equilibrium calculators that implement the MELTS, pMELTS, MELTS+DEW, and Stixrude-Lithgow-Bertelloni thermodynamic model/data collections. Examples in Jupyter notebooks demonstrate how to call the class methods using Python wrappers.

This README explains how to compile the package and to build the Python wrappers. For API documentation, see the [Thermoengine documentation](https://enki-portal.gitlab.io/ThermoEngine/).      

Please see the README file in the Cluster folder for details on how to build and deploy the ThermoEngine package on Google cloud.

Please see the README files in the Notebooks folder for descriptions of Jupyter notebooks that illustrate various uses and capabilities of the ThermoEngine package.

If you are interested in contributing to this software project, please consult the CONTRIBUTING document.

Repository configuration and maintenance, and the relationship of this repository to others in the ENKI-portal group, is described in the MAINTENANCE document.

This software is released under the GNU Affero General Public License ([GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)).

## Contents  
[Building and installing the code and testers](#building-and-installing-the-code-and-testers)
- [Prerequisites](#prerequisites)  
- [Compilation](#compilation)  
- [Installation](#installation)   
- [Xcode builds](#xcode-builds)   

[Docker image](#docker-image)  
[Building the Python package](#building-the-python-package)  
[Examples (Jupyter notebooks)](#examples-jupyter-notebooks)  


## Building and installing the code and testers  
### Prerequisites
The build requires that the `gsl` library is installed.  This library can be obtained from Homebrew (Macintosh) or downloaded directly from [GNU](https://www.gnu.org/software/gsl/). The build assumes that the `gsl` library is installed into **/usr/local**.

* NOTE: the gsl library downloaded using pip or conda do not function (perhaps because they place the library in the wrong location). Instead using Homebrew to place the gsl library in the correct location.

### Compilation
You can compile the Objective-C library in the root project directory by executing the command:
```
make
```
### Installation
You can install the library into **/usr/local/lib** (where Python can locate it) with the command:
```
make install
```
You can install the **thermoengine** python interface package with the command:
```
make pyinstall
```
### Testing and code coverage
```
make tests
```
### Xcode builds
You can also build the package on Macintosh systems using Xcode.  Launch Xcode and open the ThermoEngine.workspace project.  Schema are provided to build dynamic libraries and test routines.  
## Docker Image
A fully configured and working executable of the project software is available as a Docker image. The image is named
```
registry.gitlab.com/enki-portal/thermoengine:latest
```
Please consult the README file in the Cluster directory for instructions on running this image.
## Building the Python package
Accessing the Objective-C library from Python requires installing the Rubicon-ObjC package. You can easily install this package if you have the Anaconda package.  

If Anaconda is installed and if the root environment is based upon Python 2.7, you must create a new environment based on Python 3.7+ in order to install the ThermoEngine package. For example, execute these commands from a terminal window to create a new environment (python37) and to activate that Python 3.7 environment:
```
conda create -n python37 python=3.7 anaconda
source activate python37
```
Any Python 3.7+ environment works with Rubicon. Simply substitute the correct version numbers in the above commands.
  If you want to switch back to the original Python 2.7 environment, execute this command:
```
source activate root
```
Execute the following to install the latest version of Rubicon-ObjC 2.10 from [pypi](https://pypi.python.org/pypi):
```
pip install rubicon-objc==0.2.10
```
 Do not install a later version of rubicon-objc, as these are incompatible with the ThermoEngine Python package. Once installed, the package is available to Jupyter notebooks running the Python 3.7+ kernel on your local machine.  

 If you have a traditional Python 3.7+ installation, the above `pip` command will also install Rubicon, but additional Python dependencies may require manual installation.

## Examples (Jupyter notebooks)  
In the Notebooks folder, there are numerous Jupyter notebooks that demonstrate how to call the class methods using Python wrappers. For more details, see the [ThermoEngine documentation](https://enki-portal.gitlab.io/ThermoEngine/examples.html).
