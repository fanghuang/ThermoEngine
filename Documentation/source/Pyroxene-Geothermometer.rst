
Pyroxene Geothermometer
=======================

Using opx and cpx solution models from Sack and Ghiorso (1994a, b, c):
----------------------------------------------------------------------

| Sack RO, Ghiorso MS (1994a) Thermodynamics of multicomponent
  pyroxenes: I. Formulation of a general model. *Contrib Mineral Petrol*
  116, 277-286
| Sack RO, Ghiorso MS (1994b) Thermodynamics of multicomponent
  pyroxenes: II. Phase relations in the quadrilateral. *Contrib Mineral
  Petrol* 116, 287-300
| Sack RO, Ghiorso MS (1994c) Thermodynamics of multicomponent
  pyroxenes: III. Calibration of Fe2+(Mg)-1, TiAl(MgSi)-1,
  TiFe3+(MgSi)-1, AlFe3+(MgSi)-1, NaAl(CaMg)-1, Al2(MgSi)-1 and Ca(Mg)-1
  exchange reactions between pyroxenes and silicate melts. *Contrib
  Mineral Petrol* 118, 271-296

Initialize some required packages, and load the phase library.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    from ctypes import cdll
    from ctypes import util
    from rubicon.objc import ObjCClass, objc_method
    cdll.LoadLibrary(util.find_library('phaseobjc'))




.. parsed-literal::

    <CDLL '/usr/local/lib/libphaseobjc.dylib', handle 7f83b87052e0 at 0x10e66f278>



Define some conversion functions that …
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

… take dictionaries of oxide names and oxides values and return
molecular weights and arrays of molar concentrations.

.. code:: ipython3

    def oxide_mw (formulas=["H2O"]):
        result = {}
        PhaseBase = ObjCClass('PhaseBase')
        for formula in formulas:
            obj = PhaseBase.alloc().init()
            obj.setPhaseFormula_(formula)
            result[formula]= obj.mw
        return result
    
    import ctypes
    def oxides_wts_to_element_moles (oxides={"H2O" : 100.0}):
        e = (ctypes.c_double*107)()
        ctypes.cast(e, ctypes.POINTER(ctypes.c_double))
        for i in range (0, 107):
            e[i] = 0.0
        PhaseBase = ObjCClass('PhaseBase')
        for formula, value in oxides.items():
            obj = PhaseBase.alloc().init()
            obj.setPhaseFormula_(formula)
            moles = value/obj.mw
            elements = obj.formulaAsElementArray
            for i in range (0, 107):
                coeff = elements.valueAtIndex_(i)
                if coeff != 0.0:
                    e[i] += coeff*moles
        return e
    
    def element_moles_to_pyx_moles(e):
        m = (ctypes.c_double*nc)()
        ctypes.cast(m, ctypes.POINTER(ctypes.c_double))
        p = 2000.0
        Na = 11
        Mg = 12
        Al = 13
        Si = 14
        Ca = 20
        Ti = 22
        Cr = 24
        Mn = 25
        Fe = 26
        sumcat  = e[Na] +     e[Mg] +     e[Al] +     e[Si] +     e[Ca] +     e[Ti] +     e[Cr] +     e[Mn] + e[Fe]
        sumchg  = e[Na] + 2.0*e[Mg] + 3.0*e[Al] + 4.0*e[Si] + 2.0*e[Ca] + 4.0*e[Ti] + 3.0*e[Cr] + 2.0*e[Mn]
        if e[Na]+e[Ca] > 0.25*sumcat:
            corrSi = 4.0*(e[Na]+e[Ca]) - sumcat
        else: 
            corrSi = 0.0
        sumcat += corrSi;
    
        # catch low-P oxidized samples and acmites
        if (p < 1000.0) or (e[Na] > e[Al]): 
            fe3 = 3.0*sumcat - sumchg - 2.0*e[Fe]
            fe2 = e[Fe] - fe3
            if fe3 < 0.01*e[Fe]:
                fe3 = 0.01*e[Fe]
                fe2 = 0.99*e[Fe]
            if fe2 < 0.01*e[Fe]:
                fe2 = 0.01*e[Fe]
                fe3 = 0.99*e[Fe]
        else:
            fe2 = e[Fe]
            fe3 = 0.0
    
        m[0] = -fe3/2.0 - fe2 - e[Mn] - e[Al]/2.0 - e[Cr]/2.0 + e[Ca] + e[Na]/2.0 - e[Ti]
        m[1] =  fe3/4.0 + fe2/2.0 + e[Mn]/2.0 + e[Al]/4.0 + e[Cr]/4.0 - e[Ca]/2.0 + e[Mg]/2.0 - e[Na]/4.0
        m[2] =  fe2 + e[Mn]
        m[3] = -fe3/2.0 + e[Al]/2.0 + e[Cr]/2.0 - e[Na]/2.0 + e[Ti]
        m[4] =  fe3/2.0 - e[Al]/2.0 - e[Cr]/2.0 + e[Na]/2.0 + e[Ti]
        m[5] =  fe3/2.0 + e[Al]/2.0 + e[Cr]/2.0 - e[Na]/2.0 - e[Ti]
        m[6] =  e[Na]
        return m

Test the oxide formula to molecular weight method.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    print (oxide_mw(["Al2O3", "SiO2"]))


.. parsed-literal::

    {'Al2O3': 101.96127999999999, 'SiO2': 60.0843}


Implement a two-pyroxene geothermometer.
========================================

Reference pyroxene compositions from the Bishop Tuff (Hildreth, 1977).
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+--------+-------+-------+--------+-----+-----+-----+-----+-------+
| Phase  | SiO2  | TiO2  | Al2O3  | FeO | MnO | MgO | CaO | Na2O  |
+========+=======+=======+========+=====+=====+=====+=====+=======+
| cpx    | 51.94 | 0.711 | 0.1507 | 12. | 0.5 | 12. | 20. | 0.381 |
|        | 61538 | 15384 | 69231  | 808 | 569 | 695 | 633 | 15384 |
|        | 5     | 6     |        | 461 | 230 | 769 | 076 | 6     |
|        |       |       |        | 54  | 77  | 23  | 92  |       |
+--------+-------+-------+--------+-----+-----+-----+-----+-------+
| :math: | 0.325 | 0.159 | 0.0254 | 0.3 | 0.0 | 0.2 | 0.2 | 0.015 |
| `\sigm | 24546 | 80805 | 43754  | 057 | 388 | 509 | 004 | 83083 |
| a`     | 9     | 8     |        | 540 | 606 | 848 | 349 | 7     |
|        |       |       |        | 49  | 98  | 29  | 12  |       |
+--------+-------+-------+--------+-----+-----+-----+-----+-------+
| opx    | 50.92 | 0.425 | 0.1288 | 28. | 1.1 | 18. | 0.9 | 0.025 |
|        | 92592 | 55555 | 88889  | 495 | 037 | 330 | 796 | 18518 |
|        | 6     | 6     |        | 185 | 037 | 370 | 296 | 5     |
|        |       |       |        | 19  | 04  | 37  | 3   |       |
+--------+-------+-------+--------+-----+-----+-----+-----+-------+
| :math: | 0.460 | 0.107 | 0.0239 | 0.4 | 0.0 | 0.2 | 0.0 | 0.007 |
| `\sigm | 32535 | 64376 | 12233  | 932 | 451 | 572 | 229 | 00020 |
| a`     | 3     | 2     |        | 339 | 619 | 410 | 517 | 3     |
|        |       |       |        | 93  | 43  | 05  | 02  |       |
+--------+-------+-------+--------+-----+-----+-----+-----+-------+

Values in wt%. Averages and standard deviations computed from analyzed
pyroxenes found in the late eruptive units.

Instantiate a clinopyroxene with the specified composition.
-----------------------------------------------------------

| As an illustration of use, compute and print properties at 800 °C and
  200 MPa. Properties are output as a Python dictionary.
| Output the number of components, their names, and their formulas.

.. code:: ipython3

    CpxBerman = ObjCClass('CpxBerman')
    cpx = CpxBerman.alloc().init()
    nc = cpx.numberOfSolutionComponents()
    e = oxides_wts_to_element_moles ({'SiO2':51.94615385, 'TiO2':0.711153846, 'Al2O3':0.150769231, 'FeO':12.80846154, 
                                      'MnO':0.556923077, 'MgO':12.69576923, 'CaO':20.63307692, 'Na2O':0.381153846})
    mCpx = element_moles_to_pyx_moles(e)
    
    if (cpx.testPermissibleValuesOfComponents_(mCpx) == 1):
        print ('Cpx composition is feasible')
    else:
        print ('Cpx composition is infeasible')
        
    t = 1073.15 # K
    p = 2000.0  # bars
    potential = cpx.getChemicalPotentialFromMolesOfComponents_andT_andP_(mCpx, t, p)
    
    for i in range (0, nc):
        component = cpx.componentAtIndex_(i)
        print("{0:>20s}{1:15.2f}".format(component.phaseName, potential.valueAtIndex_(i)))


.. parsed-literal::

    Cpx composition is feasible
                diopside    -3466683.65
          clinoenstatite    -3352376.72
            hedenbergite    -3150901.60
       alumino-buffonite    -3575864.14
               buffonite    -3151406.39
                essenite    -3221797.55
                 jadeite    -3336129.22


Instantiate an orthopyroxene with the specified composition.
------------------------------------------------------------

| As an illustration of use, compute and print properties at 800 °C and
  200 MPa. Properties are output as a Python dictionary.
| Output the number of components, their names, and their formulas.

.. code:: ipython3

    OpxBerman = ObjCClass('OpxBerman')
    opx = OpxBerman.alloc().init()
    nc = opx.numberOfSolutionComponents()
    e = oxides_wts_to_element_moles ({'SiO2':50.92925926, 'TiO2':0.425555556, 'Al2O3':0.128888889, 'FeO':28.49518519, 
                                      'MnO':1.103703704, 'MgO':18.33037037, 'CaO':0.97962963, 'Na2O':0.025185185})
    mOpx = element_moles_to_pyx_moles(e)
    
    if (opx.testPermissibleValuesOfComponents_(mOpx) == 1):
        print ('Opx composition is feasible')
    else:
        print ('Opx composition is infeasible')
        
    t = 1073.15 # K
    p = 2000.0  # bars
    potential = opx.getChemicalPotentialFromMolesOfComponents_andT_andP_(mOpx, t, p)
    
    for i in range (0, nc):
        component = opx.componentAtIndex_(i)
        print("{0:>20s}{1:15.2f}".format(component.phaseName, potential.valueAtIndex_(i)))


.. parsed-literal::

    Opx composition is feasible
                diopside    -3470369.82
          clinoenstatite    -3356457.02
            hedenbergite    -3153990.55
       alumino-buffonite    -3556103.19
               buffonite    -3144518.00
                essenite    -3487657.71
                 jadeite    -3582560.72


Define an Fe-Mg exchange reaction between opx and cpx.
------------------------------------------------------

CaMgSi2O6 [cpx] + CaFeSi2O6 [opx] = CaMgSi2O6 [opx] +CaFeSi2O6 [cpx]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Note that the ``get_properties`` function for the class instance returns
a Python dictionary. The chemical potential of the endmember components
are retrieved from this dictionary by using the name of the component as
a key. Otherwise, the other thermodynamic properties are extensive (mass
dependent) quantities and pertain to the phase as a whole.

.. code:: ipython3

    def deltaG(t, p):
        cpxPotentials = cpx.getChemicalPotentialFromMolesOfComponents_andT_andP_(mCpx, t, p)
        opxPotentials = opx.getChemicalPotentialFromMolesOfComponents_andT_andP_(mOpx, t, p)
        return opxPotentials.valueAtIndex_(0) + cpxPotentials.valueAtIndex_(2) - cpxPotentials.valueAtIndex_(0) - opxPotentials.valueAtIndex_(2)

The Gibbs free energy computed by the ``deltaG`` function defined above
must be zero at equilibrium. In order to find this zero, we . . . ## . .
. import a minimizer routine from SciPy called *BrentQ.* We will use
BrentQ to find the temperature that zeroes the Gibbs free energy of a
reaction within a specified range of values.

.. code:: ipython3

    from scipy.optimize import brentq

Solve for the temperature that zeroes the exchange free energy.
---------------------------------------------------------------

Upper and lower bounds on T are specified by Tmin and Tmax (both in K).
The pressure is specified in bars.

.. code:: ipython3

    Tmin = 500.0
    Tmax = 1500.0
    p = 2000.0
    print ('Equilibrium T (°C) = ', brentq(deltaG, Tmin, Tmax, args=(p)) - 273.15)


.. parsed-literal::

    Equilibrium T (°C) =  695.2941439766115

