Codegen Examples (Jupyter notebooks)
************************************

The **Notebooks/Codegen** folder contains Jupyter notebooks that generate code for the example notebooks. They are in development and subject to change.


    Modules used:  
        * Coder
        * Coder Templates       

- Berman-plus-BM

- Berman-std-state

- Berman-SymPy-Testing

  Demonstrates how to create C-code for calculating standard state properties using the model framework of Berman (1988).

- Convergent-Ordering-Solution

  Not yet implemented.

- Helmholtz-to-Gibbs

- Non-convergent-Ordering-Solution

- Simple-Solution

- Simple-Solution-SymPy-Testing

  Demonstrates how to create C-code for calculating n-component solution properties using a simple configurational entropy model and excess enthalpies defined by an asymmetric sub-regular solution with ternary terms. Generates code for a feldspar solid solution as an example implementation.

- Stixrude-Debye
