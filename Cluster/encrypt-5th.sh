#!/bin/bash
echo Upgrade jupyterhub for enki cluster using Helm and config-*.yaml
echo Stable release is 0.8.2, upgrading to development release 0.9.0-beta.4.n008.hb20ad22
echo Upgrade to add nbgitpuller, https encryption and gitlab authentication ...
helm upgrade jhub jupyterhub/jupyterhub \
  --version=0.9.0-beta.4.n008.hb20ad22 \
  --values config-encrypt.yaml